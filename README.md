# README

### Summary

For ease of use I have deployed the project to **heroku** at `https://directlee.herokuapp.com`; directions are also included below for installing and running locally.

#### Decision making process
> Many of the decisions I made here were based on time constraints (Trying to build this Sunday evening), but here goes.
> 
> I created a base Rails API project, stripped down out minitest and replaced it with RSpec.  I did this since I'm more familiar with RSpec, and generally it seems that RSpec has more mindshare with Rails developers.
> 
> For the project itself I created two primary models; Link and URL.
> By separating this out into two classes it makes it simple to auto-generate unique slugs while also supporting custom slugs.
> 
> I simply used the id column on the URL table and do a to_s(36) on the ID field to generate slugs.  This results in case-insensitive ASCII strings made of the numerals 0-9 and letters A-Z.  More than 1 million unique combinations can fit into just 4 characters.
> 
> Comments were added to be compatible with Yard and docs were generated and checked in for convenience (though usually I would not check-in the Yard generated docs)
> 
> After cloning the repo you can can view docs with `yard server` which by default will open on port 8808.
> 
> A lot of decisions were made here in the name of getting this done quick, so here are a few things I would have done differently given more time.
> 
> * Bolted on some type of authentication and added a client model that is associated with the Link model.  Right now anyone can create/delete any link.  Fine for a demo, no good for a real production app.
> 
> * Added a Visitor model and start doing some analytics tracking.  How many times were links clicked, maybe add in some IP Geocoding and/or support for UTM parameters
> 
> * Pulled in the [discard gem](https://github.com/jhawthorn/discard) and switched to soft deletes instead of hard deletes. The benefits here would be restoring links if a customer accidentally deletes and preventing re-using slugs too quickly as this could cause a skew in analytics if old traffic is still coming through an old campaign.
> 
> * Validated Destination URLs


### Using the API from curl

* Create a link from curl _with_ custom slug

`curl --location --request POST 'https://directlee.herokuapp.com/links/create?destination=https://www.goldbelly.com&slug=yum'`

* Create a link from curl _without_ slug

`curl --location --request POST 'https://directlee.herokuapp.com/links/create?destination=https://www.goldbelly.com'`

* Delete a link from curl

`curl --location --request DELETE 'https://directlee.herokuapp.com/links/delete/:link_id'`

* Visit a shortened URL curl

`curl --location --request GET 'https://directlee.herokuapp.com/:slug'`

or

* Visit the URL in the browser

`https://directlee.herokuapp.com/:slug`

* Maybe try one of these in the browser

[https://directlee.herokuapp.com/yum](https://directlee.herokuapp.com/yum)

[https://directlee.herokuapp.com/yolo](https://directlee.herokuapp.com/yolo)


### Building Project & Running Test suite

1. Clone the project `git clone https://gitlab.com/jseeley/shortly.git`
2. Move into the project directory `cd shortly`
3. Run the database migrations `rails db:migrate`
4. Run the test suite `rspec`
5. Run the server locally `rails s`

