Rails.application.routes.draw do
  post "/links/create", to: "links#create"
  delete "/links/delete/:id", to: "links#delete"
  get "*slug", to: "links#forward"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
