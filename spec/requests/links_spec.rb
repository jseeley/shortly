require 'rails_helper'

RSpec.describe "Links", type: :request do
  it 'creates a link' do
    headers = { "ACCEPT" => "application/json" }
    post "/links/create", params: { destination: 'https://www.goldbelly.com' }

    expect(response.content_type).to match(/application\/json/)
    expect(response).to have_http_status(:created)
  end

  FIRST_SLUG = 1
  it 'redirects short links' do
    headers = { "ACCEPT" => "application/json" }
    post "/links/create", params: { destination: 'https://www.goldbelly.com' }

    get "/#{FIRST_SLUG}"
    expect(response.body).to match(/https:\/\/www.goldbelly.com/)
    expect(response).to have_http_status(:moved_permanently)
  end

  it 'allows deleting Links by slug' do
    headers = { "ACCEPT" => "application/json" }
    post "/links/create", params: { destination: 'https://www.goldbelly.com' }

    delete "/links/delete/#{FIRST_SLUG}"
    expect(response).to have_http_status(:ok)
  end

  it 'creates a link with a premium slug' do
    headers = { "ACCEPT" => "application/json" }
    post "/links/create", params: { destination: 'https://www.goldbelly.com', slug: 'yummy' }

    body = JSON.parse(response.body)
    expect(body['slug']).to eq('yummy')
  end

  it 'does not allow creation of duplicate slugs' do
    headers = { "ACCEPT" => "application/json" }
    post "/links/create", params: { destination: 'https://www.goldbelly.com', slug: 'yummy' }
    expect(response).to have_http_status(:created)

    expect do
      post "/links/create", params: { destination: 'https://www.goldbelly.com', slug: 'yummy' }
    end.to raise_error(RuntimeError, /The requested URL is not available/)
  end
end
