require 'rails_helper'

RSpec.describe Link, type: :model do
  describe "initialization" do
    let(:link) { create(:link, slug: 'foo') }

    it 'should raise an exception if requested slug already exists' do
      expect(link.slug).to eq('foo')
      expect { Link.shorten('dest', 'foo') }.to raise_error(RuntimeError, /The requested URL is not available/)
    end

    it 'should return a link with premium slug if slug does not exist' do
      result = Link.shorten('yahoo.com', 'ya')
      expect(result.slug).to eq('ya')
    end

    it 'should generate a slug if one is not specified' do
      result = Link.shorten('yahoo.com')
      expect(result.slug).to eq('1')
    end
  end
end
