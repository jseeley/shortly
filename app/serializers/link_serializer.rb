class LinkSerializer
  # This is a very basic hand rolled JSON serializer
  # that is used to return a response when a URL
  # is successfully shortened.
  #
  # @param link [Link] an ActiveRecord Link object
  # @param request [ActionDispath::Request] take in the request object to build the short URL
  # @return [Hash] The resulting hash object that gets the Link and it's associated URL object
  def self.serialize(link, request)
    link_hash = link.as_json.slice('id', 'slug')
    link_hash['short_url'] = "#{request.base_url}/#{link_hash['slug']}"
    url_hash = link.url.as_json.slice('destination')
    link_hash = link_hash.merge(url_hash).merge(ok: true)
  end
end
