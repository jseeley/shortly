class Link < ApplicationRecord
  has_one :url, dependent: :destroy

  # Takes in a destination url and an optional slug.
  # Returns a link object on success or raises an exception
  # if a requested slug already exists.
  #
  # It allows duplicate URLs to be created rather than
  # rejecting or returning the existing URL. I considered
  # doing otherwise, but if this were being used for some type
  # of campaign analytics it is possible the client would want
  # multiple slugs all pointing to the same destination.
  #
  # @param dest [String] the destination URL
  # @param slug [String, nil] a premium slug or nil
  # @return [Link] the resulting Link object
  def self.shorten(dest, slug=nil)
    # TODO: Possibly add a client_id

    link = Link.find_by(slug: slug)
    raise 'The requested URL is not available' if link

    if link
      return link
    else
      link = Link.new
      link.slug = slug
      link.save!

      begin
        retries ||= 0
        url = Url.new
        url.link = link
        url.destination = dest
        url.save

        if slug.nil?
          link.slug = url.id.to_s(36)
          link.save!
        end
      rescue
        url.delete
        retry if (retries += 1) < 3
      end

      return link
    end
  end

end
