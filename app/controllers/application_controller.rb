class ApplicationController < ActionController::API
  # TODO: This is here to provide some basic authentication
  # may want to explore reducing risk for timing attacks.
  #
  # Good article here on this topic
  # https://thoughtbot.com/blog/token-authentication-with-rails
  include ::ActionController::HttpAuthentication::Token::ControllerMethods
end
