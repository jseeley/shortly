class LinksController < ApplicationController
  def create
    link = Link.shorten(params[:destination], params[:slug])
    render json: LinkSerializer.serialize(link, request), status: 201
  end

  def delete
    Link.destroy(params[:id])
    head :ok
  end

  def forward
    link = Link.find_by(slug: params[:slug])
    if link
      redirect_to link.url.destination, status: :moved_permanently
    else
      head 404
    end
  end
end
